#ifndef CPAINT_H_
# define CPAINT_H_

# define HEIGTH	600
# define WIDTH	800
# define DEPTH	-10.0f
# define DFIELD 45.0f

enum e_Choix {ColorRed = 0, ColorGreen = 1, ColorBlue = 2, ColorSpeed, CaseSize} typedef t_Choix;

void drawScene();
void handleResize(int w, int h);
void reset();
void handleMotion(int x, int y);
void handleMouse(int key, int state, int x, int y);
void handleKeypress(unsigned char key, int x, int y);
void handleSpecialKeypress(int key, int x, int y);

#endif /* !CPAINT_H_ */
