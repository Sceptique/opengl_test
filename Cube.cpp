#include "GL/glut.h"
#include "Cube.hpp"

Cube::Cube(float size, float red, float green, float blue, bool degradation) :
  _red(red), _green(green), _blue(blue), _size(size), _deg(degradation) {}
Cube::~Cube() {}
float Cube::red() { return _red; }
float Cube::red(float i) { return _red = i; }
float Cube::green() { return _green; }
float Cube::green(float i) { return _green = i; }
float Cube::blue() { return _blue; }
float Cube::blue(float i) { return _blue = i; }
float Cube::size() { return _size; }
float Cube::size(float i) { return _size = i; }

void Cube::display(bool enable_swap) {
    // front
  glColor3f(_red, _green, _blue);
  glBegin(GL_QUADS);
  glVertex3f(_size, _size, 0.0f);
  glVertex3f(_size, 0.0f, 0.0f);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glVertex3f(0.0f, _size, 0.0f);
  glEnd();

  // back
  if (_deg)
    glColor3f(_red-0.3, _green-0.3, _blue-0.3);
  glBegin(GL_QUADS);
  glVertex3f(_size, _size, _size);
  glVertex3f(_size, 0.0f, _size);
  glVertex3f(0.0f, 0.0f, _size);
  glVertex3f(0.0f, _size, _size);
  glEnd();

  // top
  if (_deg)
    glColor3f(_red+0.2, _green+0.2, _blue+0.2);
  glBegin(GL_QUADS);
  glVertex3f(_size, _size, 0.0f);
  glVertex3f(_size, _size, _size);
  glVertex3f(0.0f, _size, _size);
  glVertex3f(0.0f, _size, 0.0f);
  glEnd();

  // bottom
  if (_deg)
    glColor3f(_red-0.25, _green-0.25, _blue-0.25);
  glBegin(GL_QUADS);
  glVertex3f(_size, 0.0f, 0.0f);
  glVertex3f(_size, 0.0f, _size);
  glVertex3f(0.0f, 0.0f, _size);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glEnd();

  // right
  if (_deg)
    glColor3f(_red-0.1, _green-0.1, _blue-0.1);
  glBegin(GL_QUADS);
  glVertex3f(_size, _size, _size);
  glVertex3f(_size, 0.0f, _size);
  glVertex3f(_size, 0.0f, 0.0f);
  glVertex3f(_size, _size, 0.0f);
  glEnd();

  // left
  if (_deg)
    glColor3f(_red-0.15, _green-0.15, _blue-0.15);
  glBegin(GL_QUADS);
  glVertex3f(0.0f, _size, _size);
  glVertex3f(0.0f, 0.0f, _size);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glVertex3f(0.0f, _size, 0.0f);
  glEnd();

  if (enable_swap)
    glutSwapBuffers();
}

void Cube::move(float x, float y, float z) {
  glTranslatef(x, y, z);
}

void create_square(float c1, float c2, float c3) {
  glColor3f(c1, c2, c3);
  glBegin(GL_QUADS);
  glVertex3f(1.0f, 1.0f, -0.0f);
  glVertex3f(1.0f, 0.0f, -0.0f);
  glVertex3f(0.0f, 0.0f, -0.0f);
  glVertex3f(0.0f, 1.0f, -0.0f);
  glEnd();
}

void create_cube(float c1, float c2, float c3, float size) {
  // front
  glColor3f(c1, c2, c3);
  glBegin(GL_QUADS);
  glVertex3f(size, size, 0.0f);
  glVertex3f(size, 0.0f, 0.0f);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glVertex3f(0.0f, size, 0.0f);
  glEnd();

  // back
  glColor3f(c1-0.3, c2-0.3, c3-0.3);
  glBegin(GL_QUADS);
  glVertex3f(size, size, size);
  glVertex3f(size, 0.0f, size);
  glVertex3f(0.0f, 0.0f, size);
  glVertex3f(0.0f, size, size);
  glEnd();

  // top
  glColor3f(c1+0.2, c2+0.2, c3+0.2);
  glBegin(GL_QUADS);
  glVertex3f(size, size, 0.0f);
  glVertex3f(size, size, size);
  glVertex3f(0.0f, size, size);
  glVertex3f(0.0f, size, 0.0f);
  glEnd();

  // bottom
  glColor3f(c1-0.25, c2-0.25, c3-0.25);
  glBegin(GL_QUADS);
  glVertex3f(size, 0.0f, 0.0f);
  glVertex3f(size, 0.0f, size);
  glVertex3f(0.0f, 0.0f, size);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glEnd();

  // right
  glColor3f(c1-0.1, c2-0.1, c3-0.1);
  glBegin(GL_QUADS);
  glVertex3f(size, size, size);
  glVertex3f(size, 0.0f, size);
  glVertex3f(size, 0.0f, 0.0f);
  glVertex3f(size, size, 0.0f);
  glEnd();

  // left
  glColor3f(c1-0.15, c2-0.15, c3-0.15);
  glBegin(GL_QUADS);
  glVertex3f(0.0f, size, size);
  glVertex3f(0.0f, 0.0f, size);
  glVertex3f(0.0f, 0.0f, 0.0f);
  glVertex3f(0.0f, size, 0.0f);
  glEnd();
}
