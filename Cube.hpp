#ifndef CUBE_H_
# define CUBE_H_

class Cube
{
public:
  Cube(float size, float red = 0, float green = 0, float blue = 0, bool degradation = true);
  virtual ~Cube();
  float red();
  float red(float i);
  float green();
  float green(float i);
  float blue();
  float blue(float i);
  float size();
  float size(float s);
  void display(bool enable_swap = false);
  void move(float x, float y, float z);

protected:
  float _red;
  float _green;
  float _blue;
  float _size;
  bool _deg;
};

#endif /* !CUBE_H_ */
